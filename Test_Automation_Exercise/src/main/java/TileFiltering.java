import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import java.text.DecimalFormat;

public class TileFiltering {
    WebDriver driver;
    private static DecimalFormat df2 = new DecimalFormat("#.##");

    public TileFiltering(WebDriver driver){
        this.driver = driver;
    }

    public void openPage(){
        driver.get("https://www.smartclient.com/smartgwt/showcase/#featured_tile_filtering");
    }

    public void fillSearchField(String s){
        driver.findElement(By.cssSelector("input[class=textItemLite][name=commonName]")).sendKeys(s);
    }

    public void setLifeSpan(int i){
        WebElement slider = driver.findElement(By.cssSelector(".hSliderThumb"));
        String sliderParameters = slider.getAttribute("style");
        String arrayOfSliderParams[] = sliderParameters.split(";");
        String arrayOfLengthText[] = arrayOfSliderParams[1].split(" ");
        String arrayOfLastSplit[] = arrayOfLengthText[2].split("p");
        double percentValue = Integer.parseInt(arrayOfLastSplit[0])/100.00;
        double paramPercent = (i/60.00) * percentValue;
        int scrollBarValue = Integer.parseInt(df2.format(paramPercent).replace(".", ""));
        Actions move = new Actions(driver);
        Action action = (Action) move.dragAndDropBy(slider, (int) (scrollBarValue - Integer.parseInt(arrayOfLastSplit[0])), 0).build();
        action.perform();
        String actualValue = driver.findElement(By.xpath("/html/body/div[3]/div/div/div/div[1]/div[1]/div[2]/div/div/div[4]/div[1]/div/div/div/div[1]/div/div/div/div[2]/div/div/div/div[3]/div/div/div/table/tbody/tr/td")).getText();
        while(!actualValue.equals(Integer.toString(i))){
            if(Integer.parseInt(actualValue) > i){
                action = (Action) move.dragAndDropBy(slider, -2, 0).build();
                action.perform();
            } else {
                action = (Action) move.dragAndDropBy(slider, 2, 0).build();
                action.perform();
            }
            actualValue = driver.findElement(By.xpath("/html/body/div[3]/div/div/div/div[1]/div[1]/div[2]/div/div/div[4]/div[1]/div/div/div/div[1]/div/div/div/div[2]/div/div/div/div[3]/div/div/div/table/tbody/tr/td")).getText();
        }
    }

    public void selectSortType(String s){
        WebElement dropdown = driver.findElement(By.xpath("/html/body/div[3]/div/div/div/div[1]/div[1]/div[2]/div/div/div[4]/div[1]/div/div/div/div[1]/div/div/div/div[4]/div/form/table/tbody/tr[2]/td[2]/table/tbody/tr/td[1]/div"));
        dropdown.click();
        WebElement sortType = driver.findElement(By.id("isc_PickListMenu_0_row_0"));
        switch (s){
            case "Animal":
                sortType.click();
                break;
            case "Life Span":
                sortType = driver.findElement(By.id("isc_PickListMenu_0_row_1"));
                sortType.click();
                break;
            case "Endangered Status":
                sortType = driver.findElement(By.id("isc_PickListMenu_0_row_2"));
                sortType.click();
                break;
        }
    }

    public void changeOrderToAscend(){
        WebElement checkbox = driver.findElement(By.cssSelector(".labelAnchor"));
        checkbox.click();
    }

    public int getComponentCount(){
        int tempNumber = 0;
        String tempSelector;
        for(int i = 0; i < driver.findElements(By.className("simpleTile")).size(); i++) {
            tempSelector = "div[eventproxy=boundList_tile_" + i + "]";
            if (driver.findElement(By.cssSelector(tempSelector)).getAttribute("aria-hidden") != null){
                if (driver.findElement(By.cssSelector(tempSelector)).getAttribute("aria-hidden").equals("false")){
                    tempNumber++;
                }
            } else {
                tempNumber++;
            }
        }
        return tempNumber;
    }

}
