import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class NestedGrid {
    WebDriver driver;
    boolean end = false;

    public NestedGrid(WebDriver driver){
        this.driver = driver;
    }

    public void openPage(){
        driver.get("https://www.smartclient.com/smartgwt/showcase/#featured_nested_grid");
        WebDriverWait wait = new WebDriverWait(driver, 10);
        //WebElement itemTable = driver.findElement(By.id("isc_EHtable"));
        wait.until(ExpectedConditions.textToBe(By.xpath("/html/body/div[3]/div/div/div/div[1]/div[1]/div[2]/div/div/div[4]/div[1]/div/div/div/div[1]/div/div/div[2]/div/table/tbody/tr[1]/td[2]/div"), "Office Paper Products"));
    }

    public void openTableEntries(){
        System.out.println(driver.findElement(By.xpath("/html/body/div[3]/div/div/div/div[1]/div[1]/div[2]/div/div/div[4]/div[1]/div/div/div/div[1]/div/div/div[2]/div/table")).findElements(By.cssSelector("tr[role=listitem]")).size());
        WebElement tbody = driver.findElement(By.xpath("/html/body/div[3]/div/div/div/div[1]/div[1]/div[2]/div/div/div[4]/div[1]/div/div/div/div[1]/div/div/div[2]/div/table/tbody"));
        WebElement tableRow;
        WebDriverWait wait = new WebDriverWait(driver, 10);
        Actions act = new Actions(driver);
        boolean isItVisible = false;
        for (int i = 1; i <= 252; i++ ){
            /*try{
                //tableRow = tbody.findElement(By.cssSelector("tr[aria-posinset="+'"' + i + '"' + "]"));
                ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", tbody.findElement(By.cssSelector("tr[aria-posinset="+'"' + i + '"' + "]")));
                System.out.println(i + ": " + tbody.findElement(By.cssSelector("tr[aria-posinset="+'"' + i + '"' + "]")).getText());
            } catch (org.openqa.selenium.StaleElementReferenceException e){
                //System.out.println("caught!");
                tbody = driver.findElement(By.xpath("/html/body/div[3]/div/div/div/div[1]/div[1]/div[2]/div/div/div[4]/div[1]/div/div/div/div[1]/div/div/div[2]/div/table/tbody"));
                tableRow = tbody.findElement(By.cssSelector("tr[aria-posinset="+'"' + i + '"' + "]"));
                System.out.println(tableRow + " text: " + tableRow.getText());
                ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", tableRow);
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }

            //tbody = driver.findElement(By.xpath("/html/body/div[3]/div/div/div/div[1]/div[1]/div[2]/div/div/div[4]/div[1]/div/div/div/div[1]/div/div/div[2]/div/table/tbody"));

            //wait.until(ExpectedConditions.visibilityOf(tbody.findElement(By.cssSelector("tr[aria-posinset="+'"' + i + '"' + "]"))));
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/

            //if(!end){
                /*if (i % 15 == 0 && i != 60){
                    driver.findElement(By.xpath("/html/body/div[3]/div/div/div/div[1]/div[1]/div[2]/div/div/div[4]/div[1]/div/div/div/div[1]/div/div/div[3]")).click();
                    act.sendKeys(Keys.PAGE_DOWN).build().perform();
                    int temp = i+1;
                    //wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/div[3]/div/div/div/div[1]/div[1]/div[2]/div/div/div[4]/div[1]/div/div/div/div[1]/div/div/div[2]/div/table/tbody/tr["+ temp +"]/td[1]"))));
                }
                if(driver.findElement(By.xpath("/html/body/div[3]/div/div/div/div[1]/div[1]/div[2]/div/div/div[4]/div[1]/div/div/div/div[1]/div/div/div[2]/div/table/tbody/tr["+ i +"]/td[2]/div")).getText().equals("Poster Strips")){
                    end = true;
                }*/
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoViewIfNeeded(true);", driver.findElement(By.xpath("/html/body/div[3]/div/div/div/div[1]/div[1]/div[2]/div/div/div[4]/div[1]/div/div/div/div[1]/div/div/div[2]/div/table/tbody/tr["+ i +"]/td[1]")));
                //wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/div[3]/div/div/div/div[1]/div[1]/div[2]/div/div/div[4]/div[1]/div/div/div/div[1]/div/div/div[2]/div/table/tbody/tr["+ i +"]/td[1]"))));

            driver.findElement(By.xpath("/html/body/div[3]/div/div/div/div[1]/div[1]/div[2]/div/div/div[4]/div[1]/div/div/div/div[1]/div/div/div[2]/div/table/tbody/tr["+ i +"]/td[1]")).click();
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoViewIfNeeded(true);", driver.findElement(By.xpath("/html/body/div[3]/div/div/div/div[1]/div[1]/div[2]/div/div/div[4]/div[1]/div/div/div/div[1]/div/div/div[2]/div[2]/div/div/div/div[1]/div/div[3]")));
                //wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html/body/div[3]/div/div/div/div[1]/div[1]/div[2]/div/div/div[4]/div[1]/div/div/div/div[1]/div/div/div[2]/div[2]/div/div/div/div[1]/div/div[3]"))));
            //System.out.println(driver.findElement(By.xpath("/html/body/div[3]/div/div/div/div[1]/div[1]/div[2]/div/div/div[4]/div[1]/div/div/div/div[1]/div/div/div[2]/div[2]/div/div/div/div[1]/div/div[3]")).isDisplayed());
            while(!isItVisible){
                ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath("/html/body/div[3]/div/div/div/div[1]/div[1]/div[2]/div/div/div[4]/div[1]/div/div/div/div[1]/div/div/div[2]/div[2]/div/div/div/div[1]/div/div[3]")));
                isItVisible = driver.findElement(By.xpath("/html/body/div[3]/div/div/div/div[1]/div[1]/div[2]/div/div/div[4]/div[1]/div/div/div/div[1]/div/div/div[2]/div[2]/div/div/div/div[1]/div/div[3]")).isDisplayed();
            }
            driver.findElement(By.xpath("/html/body/div[3]/div/div/div/div[1]/div[1]/div[2]/div/div/div[4]/div[1]/div/div/div/div[1]/div/div/div[2]/div[2]/div/div/div/div[1]/div/div[3]")).click();
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoViewIfNeeded(true);", driver.findElement(By.xpath("/html/body/div[3]/div/div/div/div[1]/div[1]/div[2]/div/div/div[4]/div[1]/div/div/div/div[1]/div/div/div[2]/div/table/tbody/tr["+ i +"]/td[1]")));
            System.out.println(i + ": " + driver.findElement(By.xpath("/html/body/div[3]/div/div/div/div[1]/div[1]/div[2]/div/div/div[4]/div[1]/div/div/div/div[1]/div/div/div[2]/div/table/tbody/tr["+ i +"]/td[2]/div")).getText());
                //System.out.println(i + ": " + driver.findElement(By.xpath("/html/body/div[3]/div/div/div/div[1]/div[1]/div[2]/div/div/div[4]/div[1]/div/div/div/div[1]/div/div/div[2]/div/table/tbody/tr["+ i +"]")));
           // }
        }
    }
}
