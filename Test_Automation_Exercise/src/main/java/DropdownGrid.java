import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DropdownGrid {
    WebDriver driver;

    public DropdownGrid(WebDriver driver){
        this.driver = driver;
    }

    public void openPage(){
        driver.get("https://www.smartclient.com/smartgwt/showcase/#featured_dropdown_grid_category");
    }

    public void openDropdown(){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement dropdown = driver.findElement(By.xpath("/html/body/div[3]/div/div/div/div[1]/div[1]/div[2]/div/div/div[4]/div[1]/div/div/div/div[1]/div/div/div/form/table/tbody/tr[2]/td[2]/table/tbody/tr/td[1]/div"));
        dropdown.click();
        boolean itemFound = false;
        Actions act = new Actions(driver);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/html/body/div[6]/div/div[2]/div/table/tbody/tr[1]/td[1]/div")));
        for(int i = 1; i <= driver.findElement(By.id("isc_40table")).findElements(By.cssSelector("tr[role=option]")).size(); i++){
            if(!itemFound) {
                if (i % 19 == 0) {
                    act.sendKeys(Keys.PAGE_DOWN).build().perform();
                    i = 1;
                }
                if (driver.findElement(By.xpath("/html/body/div[6]/div/div[2]/div/table/tbody/tr[" + i + "]/td[1]/div")).getText().contains("Exercise")) {
                    if (driver.findElement(By.xpath("/html/body/div[6]/div/div[2]/div/table/tbody/tr[" + i + "]/td[2]/div")).getText().contains("Ea")) {
                        if (Double.parseDouble(driver.findElement(By.xpath("/html/body/div[6]/div/div[2]/div/table/tbody/tr[" + i + "]/td[3]/div")).getText()) > 1.1) {
                            driver.findElement(By.xpath("/html/body/div[6]/div/div[2]/div/table/tbody/tr[" + i + "]/td[1]/div")).click();
                            itemFound = true;
                        }
                    }
                }
            }
        }
    }
}
