import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DropdownGridTests {
    WebDriver driver = new ChromeDriver();
    DropdownGrid dropdownGrid;

    @BeforeClass
    public static void setup(){
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
    }

    @Before
    public void init(){
        dropdownGrid = new DropdownGrid(driver);
    }

    @After
    public void teardown(){
        driver.close();
    }

    @Test
    public void findMatchingItem(){
        dropdownGrid.openPage();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("p[class=intro-para]")));
        dropdownGrid.openDropdown();
    }
}
