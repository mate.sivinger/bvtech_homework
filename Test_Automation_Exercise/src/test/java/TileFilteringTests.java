import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TileFilteringTests {
    WebDriver driver = new ChromeDriver();
    TileFiltering tileFiltering;

    @BeforeClass
    public static void setup(){
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
    }

    @Before
    public void init(){
        tileFiltering = new TileFiltering(driver);
        tileFiltering.openPage();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("input[class=textItemLite][name=commonName]")));
    }

    @After
    public void teardown(){
        driver.close();
    }

    @Test
    public void enterTextToSearchField(){
        tileFiltering.fillSearchField("a");
        //Assert.assertEquals("a", driver.findElement(By.xpath("/html/body/div[3]/div/div/div/div[1]/div[1]/div[2]/div/div/div[4]/div[1]/div/div/div/div[1]/div/div/div/div[2]/div/form/table/tbody/tr[2]/td[2]/input")).getText());
    }

    @Test
    public void setLifeSpan(){
        tileFiltering.setLifeSpan(40);
        Assert.assertEquals(40, Integer.parseInt(driver.findElement(By.xpath("/html/body/div[3]/div/div/div/div[1]/div[1]/div[2]/div/div/div[4]/div[1]/div/div/div/div[1]/div/div/div/div[2]/div/div/div/div[3]/div/div/div/table/tbody/tr/td")).getText()));
    }

    @Test
    public void selectDropDown(){
        tileFiltering.selectSortType("Animal");
        Assert.assertEquals("Animal", driver.findElement(By.xpath("/html/body/div[3]/div/div/div/div[1]/div[1]/div[2]/div/div/div[4]/div[1]/div/div/div/div[1]/div/div/div/div[4]/div/form/table/tbody/tr[2]/td[2]/table/tbody/tr/td[1]/div")).getText());
    }

    @Test
    public void selectAscending(){
        tileFiltering.changeOrderToAscend();
        Assert.assertEquals("checkboxTrueOver", driver.findElement(By.cssSelector("span[eventpart=valueicon]")).getAttribute("class"));
    }

    @Test
    public void resultIsMoreThanTheExpected(){
        tileFiltering.fillSearchField("a");
        tileFiltering.setLifeSpan(40);
        tileFiltering.selectSortType("Animal");
        tileFiltering.changeOrderToAscend();
        Assert.assertTrue(tileFiltering.getComponentCount() > 12);
    }
}
