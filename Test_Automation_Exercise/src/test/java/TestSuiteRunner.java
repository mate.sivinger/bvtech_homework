import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class TestSuiteRunner {
    public static void main(String[] args) {
        Result result = JUnitCore.runClasses(TestSuite.class);

        for (Failure failure : result.getFailures()){
            System.out.println(failure.toString());
        }

        System.out.println("Number of executed tests: " + result.getRunCount());
        System.out.println("Number of failed tests: " + result.getFailures().size());

        float maturity = (result.getRunCount() - result.getFailures().size());

        System.out.println("Maturity: " + Math.round((maturity/result.getRunCount())*100) + "%");

        //System.out.println(result.wasSuccessful());
    }
}