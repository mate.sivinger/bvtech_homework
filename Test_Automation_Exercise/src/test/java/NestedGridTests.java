import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class NestedGridTests {
    WebDriver driver = new ChromeDriver();
    NestedGrid nestedGrid;

    @BeforeClass
    public static void setup(){
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
    }

    @Before
    public void init(){
        nestedGrid = new NestedGrid(driver);
    }

    @After
    public void teardown(){
        //driver.close();
    }

    @Test
    public void iterateThroughNestedGrid(){
        nestedGrid.openPage();
        driver.manage().window().maximize();
        nestedGrid.openTableEntries();
    }
}
