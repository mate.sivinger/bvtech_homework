import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.Random;

public class XmlTask {
    public static void main(String[] args) {
        try{
            File fXmlFile = new File("src/main/resources/data.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);

            doc.getDocumentElement().normalize();

            System.out.println("<sportsBookReference>" + doc.getElementsByTagName("sportsBookReference").item(0).getTextContent() + "</sportsBookReference>");
            System.out.println("<transactionId>" + doc.getElementsByTagName("transactionId").item(0).getTextContent() + "</transactionId>");
            System.out.println("<outcomeId>" + doc.getElementsByTagName("outcomeId").item(0).getTextContent() + "</outcomeId>");
            System.out.println("<totalStake>" + doc.getElementsByTagName("totalStake").item(0).getTextContent() + "</totalStake>");

            int leftLimit = 97;
            int rightLimit = 122;
            int targetStringLength = 100;
            Random random = new Random();
            StringBuilder buffer = new StringBuilder(targetStringLength);
            for (int i = 0; i < targetStringLength; i++) {
                int randomLimitedInt = leftLimit + (int)
                        (random.nextFloat() * (rightLimit - leftLimit + 1));
                buffer.append((char) randomLimitedInt);
            }
            String generatedString = buffer.toString();

            NodeList nList = doc.getElementsByTagName("description");
            for (int i = 0; i < nList.getLength(); i++) {

                Node nNode = nList.item(i);

                if(i == 1){
                    nNode.setTextContent(generatedString);
                    System.out.println("<description>" + nNode.getTextContent() + "</description>");
                }
            }

            doc.getElementsByTagName("totalStake").item(0).setTextContent("10");
            System.out.println("<totalStake>" + doc.getElementsByTagName("totalStake").item(0).getTextContent() + "</totalStake>");

            doc.getElementsByTagName("transactionId").item(0).setTextContent("1");
            System.out.println("<transactionId>" + doc.getElementsByTagName("transactionId").item(0).getTextContent() + "</transactionId>");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
