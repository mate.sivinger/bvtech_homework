import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;

public class JsonTask {
    public static void main(String[] args) {
        JsonTask cornerTask = new JsonTask();
        cornerTask.printStatistics("Corners");

        JsonTask foulTask = new JsonTask();
        foulTask.printStatistics("Fouls");

        JsonTask goalKickTask = new JsonTask();
        goalKickTask.printStatistics("GoalKicks");

        JsonTask throwInTask = new JsonTask();
        throwInTask.printStatistics("ThrowIns");

        JsonTask goalTask = new JsonTask();
        goalTask.printStatistics("Goals");

        JsonTask modifFixtureId = new JsonTask();
        modifFixtureId.modifySimpleStatistics("FixtureId");

        JsonTask modifCustomer = new JsonTask();
        modifCustomer.modifySimpleStatistics("CustomerId");

        JsonTask matchTime = new JsonTask();
        matchTime.modifyMatchTime();
    }

    public void printStatistics(String statistic){
        JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(new FileReader("src/main/resources/data.json"));
            JSONObject jsonObject = (JSONObject) obj;

            JSONObject stat = (JSONObject) jsonObject.get(statistic);
            JSONObject statScore = (JSONObject) stat.get("Score");

            System.out.println(statistic + " (home): " + statScore.get("Home") + " " + statistic +" (away): " + statScore.get("Away"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void modifySimpleStatistics(String modif){
        JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(new FileReader("src/main/resources/data.json"));
            JSONObject jsonObject = (JSONObject) obj;

            switch (modif){
                case "CustomerId":
                    jsonObject.put(modif, 1);
                    //jsonObject.put(modif, 6001);
                    break;
                case "FixtureId":
                    jsonObject.put(modif, 1000);
                    //jsonObject.put(modif, 5733372);
                    break;
            }
            System.out.println(modif + ": " + jsonObject.get(modif));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void modifyMatchTime(){
        JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(new FileReader("src/main/resources/data.json"));
            JSONObject jsonObject = (JSONObject) obj;

            JSONObject matchTime = (JSONObject) jsonObject.get("StartTimes");

            matchTime.put("FirstHalf", java.time.Clock.systemUTC().instant().minusSeconds(5400));
            matchTime.put("SecondHalf", java.time.Clock.systemUTC().instant().minusSeconds(1800));

            System.out.println("FirstHalf:" + matchTime.get("FirstHalf"));
            System.out.println("SecondHalf:" + matchTime.get("SecondHalf"));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
